﻿using DTO;
using System.Collections.Generic;
using System.IO;
using System.Web.Http;
using contract;


namespace WebAPI.Controllers
{
    [RoutePrefix("api/Picture")]
    public class PictureController : ApiController
    {
        [Route("GetPicturesPerUser/{id}")]
        // GET: api/Picture/5
        public IHttpActionResult Get(int id)
        {
            var pictureDal = Factory.Resolver.GetInstance().Resolve<contract.IPictureDAL>();
            var Convert = Factory.Resolver.GetInstance().Resolve<IConvert>();
            var pram = Convert.ConvertSingleSimpleTypeToDBset(id);
            return Ok(Convert.ConvertDBsetToDTO<PictureDTO>(pictureDal.getPicturesPerUser(pram)));
        }

        // POST: api/Picture
        public void Post([FromBody]string value)
        {
        }

        [Route("UpdatePicture")]

        public IHttpActionResult Put( DTO.PictureDTO picture)
        {
            var convertString = Factory.Resolver.GetInstance().Resolve<IConvert>();
            var pictureDAL = Factory.Resolver.GetInstance().Resolve<IPictureDAL>();
            var list = new List<string>();
            list.Add(picture.pictureName);
            list.Add(picture.descriptions);
            var pram = convertString.ConvertSimpleTypeToDBset(list);
            return Ok(pictureDAL.UpdatePicture(pram));
        }
        // DELETE api/<controller>/5
        [Route("DeletePicture/{id}")]

        public IHttpActionResult Delete(int id)
        {
            var Convert = Factory.Resolver.GetInstance().Resolve<IConvert>();
            var pictureDAL = Factory.Resolver.GetInstance().Resolve<IPictureDAL>();
            var pram = Convert.ConvertSingleSimpleTypeToDBset(id);
            return Ok(pictureDAL.DeletePicture(pram));
        }
        [Route("AddPicture")]

        public IHttpActionResult AddPicture(PictureDTO p)
        {
            var Convert = Factory.Resolver.GetInstance().Resolve<IConvert>();
            var pictureDAL = Factory.Resolver.GetInstance().Resolve<IPictureDAL>();
            return Ok(pictureDAL.AddNewPicture(Convert.ConvertDTOToDBset(p)));
        }
        System.Web.HttpPostedFile hpf;
        [HttpPost]
        [Route("UploadFiles")]
        public IHttpActionResult UploadFiles()
        {

            int iUploadedCnt = 0;
            string newPath = "";
            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath;
            //sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Images/");
            sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Images/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                hpf = hfc[iCnt];
                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                    {
                        // SAVE THE FILES IN THE FOLDER.
                        //hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                        hpf.SaveAs(sPath + hpf.FileName);
                        newPath = hpf.FileName;
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }
            // RETURN A MESSAGE.
            if (iUploadedCnt >= 0)
            {
                //iUploadedCnt + " Files Uploaded Successfully";
                // return newPath;
                // return Ok (File.ReadAllBytes(newPath));
                string s = "https://localhost:44330/Images/" + hpf.FileName;
                return Ok(s);
            }
            else
            {
                return Ok("Upload Failed");
            }
        }
    }
}
