﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DTO;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/Shape")]

    public class ShapeController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getShapesByPictureId/{id}")]
        // GET: api/Picture
        public IHttpActionResult Get(int id)
        {
            contract.IConvert conversion;
            //Conversion.DataConvert<DTO.ShapeDTO> cDTO = new Conversion.DataConvert<DTO.ShapeDTO>();
            //Conversion.DataConvert<ShapeDTO> cDTO = Factory.Resolver.GetInstance().Resolve<contract.IConvert<DTO.ShapeDTO>>();‏
            try
            {
                conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert>();
                log.Info("get conversion success");
            }
            catch (Exception ex)
            {
                log.Fatal("conversion failed", ex);
                return new System.Web.Http.Results.ExceptionResult(ex, this);
            }

            contract.IShapeDAL sdal;
            try
            {
                sdal = Factory.Resolver.GetInstance().Resolve<contract.IShapeDAL>();
                log.Info("get shape dal success");

            }

            catch (Exception ex)
            {
                log.Fatal("get shape dal failed", ex);
                return new System.Web.Http.Results.ExceptionResult(ex, this);
            }


            List<System.Data.SqlClient.SqlParameter> pram;
            try
            {
                pram= conversion.ConvertSingleSimpleTypeToDBset(id);
                log.Info("conversion parm success");

            }
            catch (Exception ex)
            {
                log.Fatal("conversion parm failed", ex);
                return new System.Web.Http.Results.ExceptionResult(ex, this);
            }
            System.Data.DataSet data;
            try
            {
                data= sdal.getShapesByPictureId(pram);
                log.Info("get shapes success");

            }
            catch (Exception ex)
            {
                log.Fatal("get shapes failed", ex);
                return new System.Web.Http.Results.ExceptionResult(ex, this);
            }

            List<ShapeDTO> res;
            try
            {
                res= conversion.ConvertDBsetToDTO<DTO.ShapeDTO>(data);
                log.Info("convert result success");

            }
            catch (Exception ex)
            {
                log.Fatal("conversion failed", ex);
                return new System.Web.Http.Results.ExceptionResult(ex, this);
            }
            return Ok(res);
        }

        [Route("addShape")]
        // POST api/<controller>
        public IHttpActionResult Post(DTO.ShapeDTO shape)
        {
            //Conversion.DataConvert<DTO.ShapeDTO> cDTO = new Conversion.DataConvert<DTO.ShapeDTO>();

            //var cDTO = Factory.Resolver.GetInstance().Resolve<contract.IConvert<ShapeDTO>>();‏
            var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert>();
            //DAL.ShapeDAL sdal = new DAL.ShapeDAL();
            var sdal = Factory.Resolver.GetInstance().Resolve<contract.IShapeDAL>();
            var pram = conversion.ConvertDTOToDBset(shape);
            return Ok(sdal.addShape(pram));
        }
        [Route("addRangeShapes")]
        // POST api/<controller>
        public IHttpActionResult Post(List<DTO.ShapeDTO> shapes)
        {
            //Conversion.DataConvert<DTO.ShapeDTO> cDTO = new Conversion.DataConvert<DTO.ShapeDTO>();
            //var cDTO = Factory.Resolver.GetInstance().Resolve<contract.IConvert<ShapeDTO>>();‏
            var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert>();
            //DAL.ShapeDAL sdal = new DAL.ShapeDAL();
            var sdal = Factory.Resolver.GetInstance().Resolve<contract.IShapeDAL>();
            shapes.ForEach(s =>
            {
                var pram = conversion.ConvertDTOToDBset(s);
                sdal.addShape(pram);
            });
            return Ok();
        }
        // PUT: api/Shape/5
        [Route("UpdateShape/{id}")]

        public IHttpActionResult Put(int id, DTO.ShapeDTO line)
        {
            //Conversion.DataConvert<DTO.ShapeDTO> cDTO = new Conversion.DataConvert<DTO.ShapeDTO>();
            //var cDTO = Factory.Resolver.GetInstance().Resolve<contract.IConvert<ShapeDTO>>();‏
            var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert>();
            //DAL.ShapeDAL sdal = new DAL.ShapeDAL();
            var sdal = Factory.Resolver.GetInstance().Resolve<contract.IShapeDAL>();
            var pram = conversion.ConvertDTOToDBset(line);
            return Ok(sdal.updateShape(pram));
        }

        [Route("DeleteShape/{id}")]

        public IHttpActionResult Delete(int id)
        {
            //Conversion.DataConvert<int> cInt = new Conversion.DataConvert<int>();
            //var cInt = Factory.Resolver.GetInstance().Resolve<contract.IConvert<int>>();‏
            var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert>();
            //DAL.ShapeDAL sdal = new DAL.ShapeDAL();
            var sdal = Factory.Resolver.GetInstance().Resolve<contract.IShapeDAL>();
            var pram = conversion.ConvertSingleSimpleTypeToDBset(id);
            return Ok(sdal.deleteShape(pram));
        }
    }
}
