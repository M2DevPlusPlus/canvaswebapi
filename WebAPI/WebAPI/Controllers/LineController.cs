﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using contract;
using DTO;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/Line")]
    public class LineController : ApiController
    {   [HttpGet]
        [Route("getLineByPictureId/{id}")]
        // GET: api/Picture
        public IHttpActionResult Get(int id)
        {
            
            var Convert = Factory.Resolver.GetInstance().Resolve<IConvert>();
            var lineDal = Factory.Resolver.GetInstance().Resolve<contract.ILineDAL>();
            var pram = Convert.ConvertSingleSimpleTypeToDBset(id);
            var data = lineDal.getLinesByPictureId(pram);
            var res = Convert.ConvertDBsetToDTO<DTO.LineDTO>(data);
            return Ok(res);
        }

        // GET api/<controller>/5
        public string Get()
        {
            return "value";
        }

        [Route("addLine")]
        // POST api/<controller>
        public IHttpActionResult Post(DTO.LineDTO line)
        {

            var Convert = Factory.Resolver.GetInstance().Resolve<IConvert>();
            var lineDal = Factory.Resolver.GetInstance().Resolve<ILineDAL>();
            var pram = Convert.ConvertDTOToDBset(line);
            return Ok(lineDal.AddLine(pram));
        }
        [Route("addRangeLines")]
        // POST api/<controller>
        public IHttpActionResult Post(List<DTO.LineDTO> lines)
        {

            var Convert = Factory.Resolver.GetInstance().Resolve<IConvert>();
            var lineDal = Factory.Resolver.GetInstance().Resolve<ILineDAL>();
            lines.ForEach(l =>
            {
                var pram = Convert.ConvertDTOToDBset(l);
                lineDal.AddLine(pram);
            });
            return Ok();
        }
        // PUT api/<controller>/5
        [Route("UpdateLine")]

        public IHttpActionResult Put( DTO.LineDTO line)
        {
            var Convert = Factory.Resolver.GetInstance().Resolve<IConvert>();
            var lineDal = Factory.Resolver.GetInstance().Resolve<ILineDAL>();
            var pram = Convert.ConvertDTOToDBset(line);
            return Ok(lineDal.UpdateLine(pram));
        }

        // DELETE api/<controller>/5
        [Route("DeleteLine/{id}")]

        public IHttpActionResult Delete(int id)
        {
            var Convert = Factory.Resolver.GetInstance().Resolve<IConvert>();
            var lineDal = Factory.Resolver.GetInstance().Resolve<ILineDAL>();
            var pram = Convert.ConvertSingleSimpleTypeToDBset(id);
            return Ok(lineDal.DeleteLine(pram));
        }
    }
}