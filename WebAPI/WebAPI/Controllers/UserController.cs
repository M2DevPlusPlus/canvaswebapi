﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DTO;


namespace WebAPI.Controllers
{[RoutePrefix("api/User")]
    public class UserController : ApiController
    {

        // DELETE: api/User/5
        public void Delete(int id)
        {
        }
        [Route("AddNewUser")]
        public IHttpActionResult AddNewUser(UserDTO user)
        {
            var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert>();
            //var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert<UserDTO>>();‏
            //DataConvert<UserDTO> conversion = new DataConvert<UserDTO>();
            var param = conversion.ConvertDTOToDBset(user);
            var u = Factory.Resolver.GetInstance().Resolve<contract.IUserDAL>();
            var r = u.AddNewUser(param);
            return Ok(r);
        }
        [HttpPost]
        [Route("GetUser")]
        public IHttpActionResult GetUser(UserDTO user)
        {
            List<string> l = new List<string>();
            l.Add(user.userName);
            l.Add(user.userPassword);
            var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert>();

            //var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert<string>>();‏
            //DataConvert<string> conversion = new DataConvert<string>();
            //var conversionDTO = Factory.Resolver.GetInstance().Resolve<contract.IConvert<UserDTO>>();‏

            //DataConvert<UserDTO> conversionDTO = new DataConvert<UserDTO>();
            var param = conversion.ConvertSimpleTypeToDBset(l);
            var u = Factory.Resolver.GetInstance().Resolve<contract.IUserDAL>();
            var r = u.getUser(param);
            return Ok(conversion.ConvertSingleDBsetToDTO<UserDTO>(r));
        }
        [Route("DeleteUser")]
        public IHttpActionResult DeleteUser(int userId)
        {

            //DataConvert<int> conversion = new DataConvert<int>();
            var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert>();

            //var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert<int>>();‏

            var param = conversion.ConvertSingleSimpleTypeToDBset(userId);
            var u = Factory.Resolver.GetInstance().Resolve<contract.IUserDAL>();

            var r =u.DeleteUser(param);
            return Ok(r);
        }
        [Route("UpdateUser")]
        public IHttpActionResult UpdateUser(UserDTO user)
        {
            //var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert<UserDTO>>();‏
            var conversion = Factory.Resolver.GetInstance().Resolve<contract.IConvert>();

            //DataConvert<UserDTO> conversion = new DataConvert<UserDTO>();
            var param = conversion.ConvertDTOToDBset(user);
            var u = Factory.Resolver.GetInstance().Resolve<contract.IUserDAL>();

            var r = u.UpdateUser(param);
            return Ok(r);
        }
    }
}

